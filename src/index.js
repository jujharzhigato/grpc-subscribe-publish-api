
const grpc = require('grpc');
const uuidv1 = require('uuid/v1');
const notesProto = grpc.load('./dat.proto');
const dataStore = require('./dataStore');

const server = new grpc.Server();

server.addService(notesProto.NoteService.service,{
    list: (call,callback) => {
        callback(null, dataStore.notes);
    },
    insert: (call, callback) =>{
        let note = call.request;
        note.id = uuidv1();
        dataStore.notes.push(note);
        callback(null, note);
    },
    delete: (call, callback) => {
        let noteId = call.request.id;
        debugger;
        const noteIndex = dataStore.notes.findIndex(note => note.id === noteId);
        if(noteIndex===-1){
            callback(new Error('Note not found!'), null);
            return;
        }
        const deletedNote = dataStore.notes[noteIndex];
        dataStore.notes.splice(noteIndex,1);
        callback(null, deletedNote);
    }
})

server.bind('127.0.0.1:8081',grpc.ServerCredentials.createInsecure());
console.log('Server running at 127.0.0.1:8081');
server.start();
