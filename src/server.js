var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

const client = require('./client');

var dat;
var urls = []
var subscriptions = {} // subscriptions['topic1'] = [{url1},{url2}]

app.listen(8080);

app.post('/subscribe/:topic', function(req, res) {
    res.send("Subscribed to " + req.params.topic + "\n")
    if (!urls.includes(req.params.topic)) {
        urls.push(req.params.topic)
        subscriptions[req.params.topic] = []
    }

    subscriptions[req.params.topic].push(req.body.url)
    console.log(subscriptions)
});

app.post('/publish/:topic', function(req, res) {
    if (!urls.includes(req.params.topic)) res.end("Please subscribe first\n")
    subscriptions[req.params.topic].forEach(url => {
      client.insert({url: url, msg: req.body.message},(error,note)=>{
          if (error) {
              console.log(error);
              return;
          }

          console.log(note);
          res.end("Message sent\n")
      })
    })
});

app.get('/event', function(req, res) {
    client.list({},(error, notes)=>{
        dat = notes
        res.setHeader('Content-Type', 'application/json');
        res.json((dat))
    })
});
