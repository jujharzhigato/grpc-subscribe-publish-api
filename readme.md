
# Instructions
 
**terminal**:

$ npm install

$ cd src

$ node index.js


**terminal 2**:

$ cd src

$ node server.js


**terminal 3**:

$ curl -X POST -d '{ "url": "http://localhost:8080/event"}' http://localhost:8080/subscribe/topic1

$ curl -X POST -H "Content-Type: application/json" -d '{"message": "hello"}' http://localhost:8080/publish/topic1


**browser**:

http://localhost:8080/event
