const grpc = require('grpc');

const PROTO_PATH = './dat.proto';
const NoteService = grpc.load(PROTO_PATH).NoteService;
const client = new NoteService('localhost:8081',grpc.credentials.createInsecure());

module.exports = client;
